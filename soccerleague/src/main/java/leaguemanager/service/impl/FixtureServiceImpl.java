package leaguemanager.service.impl;

import leaguemanager.dao.FixtureDAO;
import leaguemanager.model.Fixture;
import leaguemanager.service.FixtureService;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by linh6_000 on 06/05/2017.
 */
@Service
public class FixtureServiceImpl implements FixtureService {

    private FixtureDAO fixtureDAO;

    public void setFixtureDAO(FixtureDAO fixtureDAO) {
        this.fixtureDAO = fixtureDAO;
    }

    public void addFixture(Fixture p) {

    }

    public void updateFixture(Fixture p) {

    }

    public List<Fixture> listFixtures() {
        return null;
    }

    public Fixture getFixtureById(int id) {
        return null;
    }

    public void removeFixture(int id) {

    }
}
