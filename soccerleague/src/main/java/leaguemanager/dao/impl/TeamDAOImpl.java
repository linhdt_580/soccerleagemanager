package leaguemanager.dao.impl;

import leaguemanager.dao.TeamDAO;
import leaguemanager.model.Team;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by linh6_000 on 06/05/2017.
 */
@Repository
public class TeamDAOImpl implements TeamDAO {

    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sf) {
        this.sessionFactory = sf;
    }

    public void addTeam(Team p) {
        Session session = this.sessionFactory.getCurrentSession();
        session.persist(p);
    }

    public void updateTeam(Team p) {
        Session session = this.sessionFactory.getCurrentSession();
        session.update(p);
    }

    public List<Team> listTeams() {
        Session session = this.sessionFactory.getCurrentSession();
        List<Team> teams = session.createQuery("from team").list();
        return teams;
    }

    public Team getTeamById(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        Team p = (Team) session.load(Team.class, new Integer(id));
        return p;
    }

    public void removeTeam(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        Team p = (Team) session.load(Team.class, new Integer(id));
        if(null != p){
            session.delete(p);
        }
    }
}
