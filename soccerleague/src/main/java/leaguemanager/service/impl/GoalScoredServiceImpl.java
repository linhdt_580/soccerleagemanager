package leaguemanager.service.impl;

import leaguemanager.dao.FixtureDAO;
import leaguemanager.dao.GoalScoredDAO;
import leaguemanager.model.GoalScored;
import leaguemanager.service.GoalScoredService;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by linh6_000 on 06/05/2017.
 */
@Service
public class GoalScoredServiceImpl implements GoalScoredService {

    private GoalScoredDAO goalScoredDAO;

    public void setGoalScoredDAO(GoalScoredDAO goalScoredDAO) {
        this.goalScoredDAO = goalScoredDAO;
    }

    public void addGoalScored(GoalScored p) {

    }

    public void updateGoalScored(GoalScored p) {

    }

    public List<GoalScored> listGoalScoreds() {
        return null;
    }

    public GoalScored getGoalScoredById(int id) {
        return null;
    }

    public void removeGoalScored(int id) {

    }
}
