package leaguemanager.dao;

import leaguemanager.model.League;
import leaguemanager.model.PlayerMatch;

import java.util.List;

/**
 * Created by linh6_000 on 06/05/2017.
 */
public interface PlayerMatchDAO {
    public void addPlayerMatch(PlayerMatch p);

    public void updatePlayerMatch(PlayerMatch p);

    public List<PlayerMatch> listPlayerMatchs();

    public PlayerMatch getLeagueById(int id);

    public void removePlayerMatch(int id);
}
