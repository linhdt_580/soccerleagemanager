package leaguemanager.service;

import leaguemanager.model.GoalScored;

import java.util.List;

/**
 * Created by linh6_000 on 06/05/2017.
 */
public interface GoalScoredService {

    public void addGoalScored(GoalScored p);

    public void updateGoalScored(GoalScored p);

    public List<GoalScored> listGoalScoreds();

    public GoalScored getGoalScoredById(int id);

    public void removeGoalScored(int id);
}
