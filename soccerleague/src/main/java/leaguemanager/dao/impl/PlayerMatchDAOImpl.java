package leaguemanager.dao.impl;

import leaguemanager.dao.PlayerMatchDAO;
import leaguemanager.model.PlayerMatch;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by linh6_000 on 06/05/2017.
 */
@Repository
public class PlayerMatchDAOImpl implements PlayerMatchDAO {

    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sf) {
        this.sessionFactory = sf;
    }

    public void addPlayerMatch(PlayerMatch p) {

    }

    public void updatePlayerMatch(PlayerMatch p) {

    }

    public List<PlayerMatch> listPlayerMatchs() {
        return null;
    }

    public PlayerMatch getLeagueById(int id) {
        return null;
    }

    public void removePlayerMatch(int id) {

    }
}
