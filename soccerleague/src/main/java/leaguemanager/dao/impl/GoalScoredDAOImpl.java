package leaguemanager.dao.impl;

import leaguemanager.dao.GoalScoredDAO;
import leaguemanager.model.GoalScored;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by linh6_000 on 06/05/2017.
 */
@Repository
public class GoalScoredDAOImpl implements GoalScoredDAO {

    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sf) {
        this.sessionFactory = sf;
    }

    public void addGoalScored(GoalScored p) {

    }

    public void updateGoalScored(GoalScored p) {

    }

    public List<GoalScored> listGoalScoreds() {
        return null;
    }

    public GoalScored getGoalScoredById(int id) {
        return null;
    }

    public void removeGoalScored(int id) {

    }
}
