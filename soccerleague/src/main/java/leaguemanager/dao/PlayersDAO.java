package leaguemanager.dao;

import leaguemanager.model.PlayerMatch;
import leaguemanager.model.Players;

import java.util.List;

/**
 * Created by linh6_000 on 06/05/2017.
 */
public interface PlayersDAO {
    public void addPlayers(Players p);

    public void updatePlayers(Players p);

    public List<Players> listPlayers();

    public Players getPlayersById(int id);

    public void removePlayers(int id);
}
