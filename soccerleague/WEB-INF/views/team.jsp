<%--
  Created by IntelliJ IDEA.
  User: linh6_000
  Date: 06/05/2017
  Time: 8:35 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Persons List</title>
</head>
<body>
<h3>Persons List</h3>
<c:forEach items="${listTeams}" var="team">
    <tr>
        <td>${team.id}</td>
        <td>${team.teamName}</td>
        <td>${team.league}</td>
    </tr>
</c:forEach>

</body>
</html>
