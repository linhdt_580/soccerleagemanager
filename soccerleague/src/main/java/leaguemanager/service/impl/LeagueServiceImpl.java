package leaguemanager.service.impl;

import leaguemanager.dao.GoalScoredDAO;
import leaguemanager.dao.LeagueDAO;
import leaguemanager.model.League;
import leaguemanager.service.LeagueService;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by linh6_000 on 06/05/2017.
 */
@Service
public class LeagueServiceImpl implements LeagueService {
    private LeagueDAO leagueDAO;

    public void setLeagueDAO(LeagueDAO leagueDAO) {
        this.leagueDAO = leagueDAO;
    }

    public void addLeague(League p) {

    }

    public void updateLeague(League p) {

    }

    public List<League> listLeagues() {
        return null;
    }

    public League getLeagueById(int id) {
        return null;
    }

    public void removeLeague(int id) {

    }
}
