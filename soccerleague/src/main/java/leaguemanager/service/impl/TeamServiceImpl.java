package leaguemanager.service.impl;

import leaguemanager.dao.PlayersDAO;
import leaguemanager.dao.TeamDAO;
import leaguemanager.model.Team;
import leaguemanager.service.TeamService;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by linh6_000 on 06/05/2017.
 */
@Service
public class TeamServiceImpl implements TeamService {
    private TeamDAO teamDAO;

    public void setTeamDAO(TeamDAO teamDAO) {
        this.teamDAO = teamDAO;
    }

    public void addTeam(Team p) {
        this.teamDAO.addTeam(p);
    }

    public void updateTeam(Team p) {
        this.teamDAO.updateTeam(p);

    }

    public List<Team> listTeams() {
        return this.teamDAO.listTeams();
    }

    public Team getTeamById(int id) {
        return this.teamDAO.getTeamById(id);
    }

    public void removeTeam(int id) {
        this.teamDAO.removeTeam(id);
    }
}
