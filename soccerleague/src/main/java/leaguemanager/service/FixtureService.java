package leaguemanager.service;

import leaguemanager.model.Fixture;

import java.util.List;

/**
 * Created by linh6_000 on 06/05/2017.
 */
public interface FixtureService {
    public void addFixture(Fixture p);

    public void updateFixture(Fixture p);

    public List<Fixture> listFixtures();

    public Fixture getFixtureById(int id);

    public void removeFixture(int id);
}
