package leaguemanager.service;

import leaguemanager.model.Team;

import java.util.List;

/**
 * Created by linh6_000 on 06/05/2017.
 */
public interface TeamService {
    public void addTeam(Team p);

    public void updateTeam(Team p);

    public List<Team> listTeams();

    public Team getTeamById(int id);

    public void removeTeam(int id);
}
