package leaguemanager.dao.impl;

import leaguemanager.dao.PlayersDAO;
import leaguemanager.model.Players;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by linh6_000 on 06/05/2017.
 */
@Repository
public class PlayersDAOImpl implements PlayersDAO {
    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sf) {
        this.sessionFactory = sf;
    }
    public void addPlayers(Players p) {

    }

    public void updatePlayers(Players p) {

    }

    public List<Players> listPlayers() {
        return null;
    }

    public Players getPlayersById(int id) {
        return null;
    }

    public void removePlayers(int id) {

    }
}
