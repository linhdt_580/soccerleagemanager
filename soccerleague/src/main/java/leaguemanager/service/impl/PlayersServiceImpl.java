package leaguemanager.service.impl;

import leaguemanager.dao.LeagueDAO;
import leaguemanager.dao.PlayerMatchDAO;
import leaguemanager.dao.PlayersDAO;
import leaguemanager.model.PlayerMatch;
import leaguemanager.model.Players;
import leaguemanager.service.PlayersService;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by linh6_000 on 06/05/2017.
 */
@Service
public class PlayersServiceImpl implements PlayersService {


    private PlayersDAO playersDAO;

    public void setPlayersDAO(PlayersDAO playersDAO) {
        this.playersDAO = playersDAO;
    }

    public void addPlayers(Players p) {

    }

    public void updatePlayers(Players p) {

    }

    public List<Players> listPlayers() {
        return null;
    }

    public Players getPlayersById(int id) {
        return null;
    }

    public void removePlayers(int id) {

    }
}
