package leaguemanager.dao.impl;

import leaguemanager.dao.LeagueDAO;
import leaguemanager.model.League;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by linh6_000 on 06/05/2017.
 */
@Repository
public class LeagueDAOImpl implements LeagueDAO {

    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sf) {
        this.sessionFactory = sf;
    }

    public void addLeague(League p) {

    }

    public void updateLeague(League p) {

    }

    public List<League> listLeagues() {
        return null;
    }

    public League getLeagueById(int id) {
        return null;
    }

    public void removeLeague(int id) {

    }
}
