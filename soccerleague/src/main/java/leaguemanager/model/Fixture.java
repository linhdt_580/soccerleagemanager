package leaguemanager.model;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by vanluong on 02/06/2017.
 */
@Entity
@Table(name = "fixture")
public class Fixture extends BaseModel {

    @Column(name="team1_score")
    private int team1Score;

    @Column(name = "team2_score")
    private int team2Score;

    @Column(name = "fixture_date")
    private Date fixtureDate;

    @ManyToMany(cascade = {CascadeType.ALL})
    @JoinTable(name = "player_match", joinColumns = {
            @JoinColumn(name = "fixture_id")},
            inverseJoinColumns = {
                @JoinColumn(name = "player_id")}
                )
    private Set<Players> playersSet=new HashSet<Players>();

    public int getTeam1Score() {
        return team1Score;
    }

    public void setTeam1Score(int team1Score) {
        this.team1Score = team1Score;
    }

    public int getTeam2Score() {
        return team2Score;
    }

    public void setTeam2Score(int team2Score) {
        this.team2Score = team2Score;
    }

    public Date getFixtureDate() {
        return fixtureDate;
    }

    public void setFixtureDate(Date fixtureDate) {
        this.fixtureDate = fixtureDate;
    }

    public Set<Players> getPlayersSet() {
        return playersSet;
    }

    public void setPlayersSet(Set<Players> playersSet) {
        this.playersSet = playersSet;
    }
}
