package leaguemanager.controller;

import leaguemanager.model.Team;
import leaguemanager.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.*;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by linh6_000 on 06/05/2017.
 */
@org.springframework.stereotype.Controller
public class TeamController {
    private TeamService teamService;

    @Autowired(required = true)
    @Qualifier(value = "teamService")
    public void setTeamService(TeamService teamService) {
        this.teamService = teamService;
    }

    @RequestMapping(value = "/teams", method = RequestMethod.GET)
    public String listPersons(Model model) {
        model.addAttribute("team", new Team());
        model.addAttribute("listTeams", this.teamService.listTeams());
        return "team";
    }
}
