package leaguemanager.service;

import leaguemanager.model.League;

import java.util.List;

/**
 * Created by linh6_000 on 06/05/2017.
 */
public interface LeagueService {
    public void addLeague(League p);

    public void updateLeague(League p);

    public List<League> listLeagues();

    public League getLeagueById(int id);

    public void removeLeague(int id);
}
