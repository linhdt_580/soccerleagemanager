package leaguemanager.model;

import javax.persistence.*;

/**
 * Created by vanluong on 02/06/2017.
 */
@Entity
@Table(name = "goal_scored")
public class GoalScored extends BaseModel {

    @Column(name = "goal_time")
    private int goalTime;

    @ManyToOne
    @JoinColumn(name = "player_id")
    private Players players;

    @ManyToOne
    @JoinColumn(name = "fixture_id")
    private Fixture fixture;

    public int getGoalTime() {
        return goalTime;
    }

    public void setGoalTime(int goalTime) {
        this.goalTime = goalTime;
    }

    public Players getPlayers() {
        return players;
    }

    public void setPlayers(Players players) {
        this.players = players;
    }

    public Fixture getFixture() {
        return fixture;
    }

    public void setFixture(Fixture fixture) {
        this.fixture = fixture;
    }
}
