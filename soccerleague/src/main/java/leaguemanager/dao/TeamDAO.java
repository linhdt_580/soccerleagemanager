package leaguemanager.dao;

import leaguemanager.model.Team;

import java.util.List;

/**
 * Created by linh6_000 on 06/05/2017.
 */
public interface TeamDAO {
    public void addTeam(Team p);

    public void updateTeam(Team p);

    public List<Team> listTeams();

    public Team getTeamById(int id);

    public void removeTeam(int id);
}
