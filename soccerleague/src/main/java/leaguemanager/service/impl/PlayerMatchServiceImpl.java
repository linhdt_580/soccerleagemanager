package leaguemanager.service.impl;

import leaguemanager.dao.GoalScoredDAO;
import leaguemanager.dao.PlayerMatchDAO;
import leaguemanager.dao.PlayersDAO;
import leaguemanager.model.PlayerMatch;
import leaguemanager.model.Players;
import leaguemanager.service.PlayerMatchService;
import leaguemanager.service.PlayersService;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by linh6_000 on 06/05/2017.
 */
@Service
public class PlayerMatchServiceImpl implements PlayerMatchService {

    private PlayerMatchDAO playerMatchDAO;

    public void setPlayerMatchDAO(PlayerMatchDAO playerMatchDAO) {
        this.playerMatchDAO = playerMatchDAO;
    }

    public void addPlayerMatch(PlayerMatch p) {

    }

    public void updatePlayerMatch(PlayerMatch p) {

    }

    public List<PlayerMatch> listPlayerMatchs() {
        return null;
    }

    public PlayerMatch getLeagueById(int id) {
        return null;
    }

    public void removePlayerMatch(int id) {

    }
}
