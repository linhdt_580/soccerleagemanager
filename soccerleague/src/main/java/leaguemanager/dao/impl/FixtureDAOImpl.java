package leaguemanager.dao.impl;

import leaguemanager.dao.FixtureDAO;
import leaguemanager.model.Fixture;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by linh6_000 on 06/05/2017.
 */
@Repository
public class FixtureDAOImpl implements FixtureDAO {

    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sf) {
        this.sessionFactory = sf;
    }

    public void addFixture(Fixture p) {

    }

    public void updateFixture(Fixture p) {

    }

    public List<Fixture> listFixtures() {
        return null;
    }

    public Fixture getFixtureById(int id) {
        return null;
    }

    public void removeFixture(int id) {

    }
}
