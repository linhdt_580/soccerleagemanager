package leaguemanager.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by vanluong on 02/06/2017.
 */
@Entity
@Table(name = "leage")
public class League extends BaseModel {

    @Column(name = "leage_name")
    private String leagueName;

    @Column(name = "logo")
    private String logo;

    @Column(name = "prize")
    private int prize;

    public String getLeageName() {
        return leagueName;
    }

    public void setLeageName(String leageName) {
        this.leagueName = leageName;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public int getPrize() {
        return prize;
    }

    public void setPrize(int prize) {
        this.prize = prize;
    }
}
